#ifndef WCANNER_H
#define WSCANNER_H

#include <netlink/genl/genl.h>
#include <linux/nl80211.h>
#include <netlink/genl/ctrl.h>
#include <limits.h>

#define SSID_ELEMENTID 0
#define BRATES_ELEMENTID 1
#define RSN_ELEMENTID 48
#define HTOPER_ELEMENTID 61

#define NL80211 "nl80211"
#define SCAN "scan"

#define RATES_MAX 8

/*
* STRUCTS & ENUMS
*/
struct trigger_results {
    int done;
    int aborted;
};

typedef enum {
    SUITS_CIPHER = 0,
    SUITS_AUTHENTIC = 1,
}suits_t;

int wscanner_init(char *device);
int wscanner_scan_provide(char *device);
int wscanner_parse_args(int argc, char **argv, char *device);

#endif