#ifndef WSPRINTER_H
#define WSPRINTER_H

#define BRATES_MAX 1024
#define QUALITY_MAX 70
#define NEXTSUIT 3
#define SUITSIZE 4
#define BSSIDSIZE 6
#define OUISIZE 3
#define BRATESMASK 0x7f
#define KB500 0.5

#define OTHER "Other"

typedef enum {
    CP_SUITE_TYPE_WEP_40 = 1,
    CP_SUITE_TYPE_TKIP = 2,
    CP_SUITE_TYPE_CCMP = 4,
    CP_SUITE_TYPE_WEP_104 = 5
} cp_suite_type_t;

typedef struct {
    int type;
    char *str;
} ciph_suite_str_t;

typedef enum {
    AUT_SUITE_TYPE_802_1X = 1,
    AUT_SUITE_TYPE_PSK = 2,
    AUT_SUITE_TYPE_FTO8021X = 3
} aut_suite_type_t;

void print_rsn_version(uint8_t **data);
void print_cipher_type(uint8_t **data);
void print_cipher_pairs(uint8_t **data);
void print_aut_suite_list(uint8_t **data);
void print_rates(double *data, int len);
void print_bssid(uint8_t *data);
void print_freq(uint32_t freq);
void print_signal_db(int32_t signal);
int print_ssid(uint8_t *ie, int ielen);
int print_channel(uint8_t *ie, int ielen);
int print_bitrates(uint8_t *ie, int ielen);
int print_rsn_info(uint8_t *ie, int ielen);

#endif