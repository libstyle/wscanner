CC=gcc

TARGET=wscanner
OBJECTS=main.o wscanner.o wsprinter.o
FLAGS=-Wall -Wno-unused-parameter -g -I/usr/include/libnl3
LDFLAGS=-lnl-genl-3 -lnl-3

$(TARGET): $(OBJECTS)
	$(CC)  $(OBJECTS) $(LDFLAGS) -o $(TARGET)

main.o: main.c
	$(CC) $(FLAGS) -c main.c

wscanner.o: wscanner.c
	$(CC) $(FLAGS) -c wscanner.c

wsprinter.o: wsprinter.c
	$(CC) $(FLAGS) -c wsprinter.c

.PHONY: clean

clean:
	rm *.o wscanner

