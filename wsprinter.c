#include <stdio.h>
#include <ctype.h>
#include "wscanner.h"
#include "wsprinter.h"

/*
 * Cipher suite to string.
 */
static char *cipher_suite2str(uint8_t type)
{
    int i;
    ciph_suite_str_t csuites[] = {

        {CP_SUITE_TYPE_WEP_40, "WEP-40"},
        {CP_SUITE_TYPE_TKIP, "TKIP"},
        {CP_SUITE_TYPE_CCMP, "CCMP"},
        {CP_SUITE_TYPE_WEP_104, "WEP-104"},
        {0, NULL}
    };

    for (i = 0; csuites[i].type != 0; i++)
    {
        if (csuites[i].type == type)
            return csuites[i].str;
    }

    return OTHER;
}

/*
 * Authentic suite to string.
 */
char *auth_suite2str(uint8_t type)
{
    int i;
    ciph_suite_str_t autsuites[] = {

        {AUT_SUITE_TYPE_802_1X, "802.1X"},
        {AUT_SUITE_TYPE_PSK, "PSK"},
        {AUT_SUITE_TYPE_FTO8021X, "FT over 802.1X"},
        {0, NULL}
    };

    for (i = 0; autsuites[i].type != 0; i++)
    {
        if (autsuites[i].type == type)
            return autsuites[i].str;
    }

    return OTHER;
}


uint8_t *get_element_id_ptr(uint8_t *ie, int ielen, int elem)
{
    int len = 0;
        /* searching ht operation element */
    while ((*ie != elem) && (len < ielen))
    {
        ie++;
        ie += *ie;
        len++;
        ie++;
    }

    if (len == ielen)
        return NULL; /* element not found = print nothing */

    return ie;
}
/*
 * Print channel.
 */
int print_channel(uint8_t *ie, int ielen)
{

    ie = get_element_id_ptr(ie, ielen, HTOPER_ELEMENTID);
    if (ie == NULL)
        return -1; /* element not found = print nothing */

    /* shift to channel value */
    ie += 2;
    printf("Channel: %d\n", *ie);

    return 0;
}

/*
 * Print ssid.
 */
int print_ssid(uint8_t *ie, int ielen)
{
    int i;
    int len = 0;

    ie = get_element_id_ptr(ie, ielen, SSID_ELEMENTID);
    if (ie == NULL)
        return -1; /* element not found = print nothing */

    printf("SSID: ");

    if (ie[1] >= 0 && ie[1] <= 32)
    {
        len = ie[1];
        ie += 2;
        for (i = 0; i < len; i++)
        {
            if (isprint(ie[i]))
                printf("%c", ie[i]);
            else
                printf("\\x%.2x", ie[i]);
        }
    }

    printf("\n");

    return 0;
}

/*
 * Print bit rates.
 */
int print_bitrates(uint8_t *ie, int ielen)
{
    int i;
    double rates[RATES_MAX];
    int len = 0;

    ie = get_element_id_ptr(ie, ielen, BRATES_ELEMENTID);
    if (ie == NULL)
        return -1; /* element not found = print nothing */

    if (ie[1] >= 0 && ie[1] <= RATES_MAX)
    {
        len = ie[1];
        ie += 2;
        for (i = 0; i < len; i++)
            rates[i] = (double)(*(ie + i) & BRATESMASK) * KB500;
    }
    else
        return -1; /* print nothing */

    printf("Supported bitrates: ");
    for (i = 0; i < len; i++)
    {
        if (i < (len - 1))
            printf(" %1.1f Mb/s;", rates[i]);
        else
            printf(" %1.1f Mb/s\n", rates[i]);
    }

    return 0;
}

/*
 * Print rsn version.
 */
void print_rsn_version(uint8_t **data)
{
    int version;

    /* data start on element id */
    /* for version data are shifted behind of length */
    version = *(*data + 3) << 8;
    version |= *(*data + 2);
    printf("WPA2 Version: %d\n", version);
    /* shift to start of group cipher */
    (*data) += 4;
}

/*
 * Print group cipher.
 */
void print_cipher_type(uint8_t **data)
{
    uint8_t ieee80211_oui[OUISIZE] = {0x00, 0x0f, 0xac};
    char *ptr;

    if (!memcmp(ieee80211_oui, *data, OUISIZE))
    {
        (*data) += 3;
        ptr = cipher_suite2str(*(uint8_t *)(*data));
        printf("Suite type: %s\n", ptr);
        (*data) += 1;
    }
    else
    {
        /* print nothing */
    }
}

/*
 * Print suits.
 */
void print_suits(uint8_t **data, int type)
{
    static char* (*suite2str)(uint8_t);
    int suit_cnt;
    char *ptr;
    int i;

    suite2str = &cipher_suite2str;

    /* get number of suits */
    suit_cnt = *(*data + 1) << 8;
    suit_cnt |= *(*data);
    /* shift to start of suit */
    (*data) += 2;
    switch (type)
    {
        case SUITS_CIPHER:
            printf("Pairwise ciphers (%d) :", suit_cnt);
            suite2str = &cipher_suite2str;
            break;

        case SUITS_AUTHENTIC:
            printf("Authentication suites (%d) :", suit_cnt);
            suite2str = &auth_suite2str;
            break;

        default: break;
    }

    for (i = 0; i < suit_cnt; i++)
    {
        ptr = suite2str(*(uint8_t *)(*data + NEXTSUIT + SUITSIZE * i));

        if (i < (suit_cnt - 1))
            printf(" %s,", ptr);
        else
            printf(" %s\n", ptr);
    }
    /* shift to next info behind suit list */
    (*data) += (suit_cnt * 4);
}

/*
 * Print security info.
 */
int print_rsn_info(uint8_t *ie, int ielen)
{

    ie = get_element_id_ptr(ie, ielen, RSN_ELEMENTID);
    if (ie == NULL)
    {
        printf("Security info: Unknown\n\n");
        return -1; /* element not found = print nothing */
    }

    printf("Security info: \n");
    print_rsn_version(&ie);
    print_cipher_type(&ie);
    print_suits(&ie, SUITS_CIPHER);
    print_suits(&ie, SUITS_AUTHENTIC);
    printf("\n\n");
    return 0;
}

/*
 * Print bssid.
 */
void print_bssid(uint8_t *data)
{
    int i, l;

    printf("BSSID: ");

    l = 0;
    for (i = 0; i < BSSIDSIZE; i++)
    {
        if (i == 0)
        {
            printf("%02x", data[i]);
            l += 2;
        }
        else
        {
            printf(":%02x", data[i]);
            l += 3;
        }
    }
    printf("\n");
}

/*
 * Print freq.
 */
void print_freq(uint32_t freq)
{
    printf("Frequency: %d MHz\n", freq);
}

/*
 * Print signal in db
 */
void print_signal_db(int32_t signal)
{
    int8_t tsig;
    int quality;

    tsig = (uint8_t)(signal / 100);
    tsig = tsig - 0x100;

    if (tsig < -110)
        tsig = -110;
    else if (tsig > -40)
        tsig = -40;

    quality = tsig + 110;
    printf("Signal:    %d db\n", tsig);
    printf("Quality:   %d/%d\n", quality, QUALITY_MAX);
}
