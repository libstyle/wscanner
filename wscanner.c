#include <errno.h>
#include <net/if.h>
#include <unistd.h>
#include "wscanner.h"
#include "wsprinter.h"

static struct socket_pars_t {
    int if_index;
    struct nl_sock *socket;
    int driver_id;
} socket_pars;

/*
 * parsing input arguments
 */
int wscanner_parse_args(int argc, char **argv, char *device)
{
    int opt = 0, stat = 0, rv = -1;

    while((opt = getopt(argc, argv, ":d:")) != -1)
    {
        switch(opt)
        {
        case ':':
            printf("missing argument for option: %s\n", argv[optind-1]);
            stat = 1;
            break;

        case 'd':
            strncpy(device, optarg,_POSIX_ARG_MAX);
            break;

        case '?':
            printf("option: %s is not supported\n", argv[optind-1]);
            stat = 1;
            break;

        default:
            break;
        }
    }
    for (; optind < argc; optind++) /* args which are not parsed */
    {
        printf("not expected arguments: %s\n", argv[optind]);
        stat = 1;
    }
    /* argument must be set */
    if (!device[0])
    {
        printf(
            "wireless interface is necessary -d 'INTERFACE'\n\n");
        stat = 1;
    }

    if (stat)
        goto Error;

    rv = stat;

Error:
    return rv;
}

/*
 * Callback for errors.
 */
static int error_handler(struct sockaddr_nl *nla, struct nlmsgerr *err, void *arg)
{
    int *ret = arg;

    printf("error_handler() called.\n");
    *ret = err->error;
    return NL_STOP;
}

/*
 * Callback for NL_CB_FINISH.
 */
static int finish_handler(struct nl_msg *msg, void *arg)
{

    int *ret = arg;

    *ret = 0;
    return NL_SKIP;
}

/*
 * Callback for NL_CB_ACK.
 */
static int ack_handler(struct nl_msg *msg, void *arg)
{
    int *ret = arg;

    *ret = 0;
    return NL_STOP;
}

/*
 * Callback for NL_CB_SEQ_CHECK.
 */
static int no_seq_check(struct nl_msg *msg, void *arg) {

    return NL_OK;
}

/*
 * Callback for trigger.
 */
static int callback_trigger(struct nl_msg *msg, void *arg)
{
    /* Called by the kernel when the scan is done or has been aborted. */
    struct genlmsghdr *gnlh = nlmsg_data(nlmsg_hdr(msg));
    struct trigger_results *results = arg;

    if (gnlh->cmd == NL80211_CMD_SCAN_ABORTED)
    {
        results->done = 1;
        results->aborted = 1;
    }
    else if (gnlh->cmd == NL80211_CMD_NEW_SCAN_RESULTS)
    {
        results->done = 1;
        results->aborted = 0;
    }

    return NL_SKIP;
}

/*
 * Callback received data handler.
 */
int callback_data_handle(struct nl_msg *msg, void *arg)
{
    /* Called by the kernel with a dump of the successful scan's data. Called for each SSID.*/
    struct genlmsghdr *gnlh = nlmsg_data(nlmsg_hdr(msg));
    struct nlattr *tb[NL80211_ATTR_MAX + 1];
    struct nlattr *bss[NL80211_BSS_MAX + 1];
    static struct nla_policy bss_policy[NL80211_BSS_MAX + 1] = {
        [NL80211_BSS_TSF] = {.type = NLA_U64},
        [NL80211_BSS_FREQUENCY] = {.type = NLA_U32},
        [NL80211_BSS_BSSID] = {},
        [NL80211_BSS_BEACON_INTERVAL] = {.type = NLA_U16},
        [NL80211_BSS_CAPABILITY] = {.type = NLA_U16},
        [NL80211_BSS_INFORMATION_ELEMENTS] = {},
        [NL80211_BSS_SIGNAL_MBM] = {.type = NLA_U32},
        [NL80211_BSS_SIGNAL_UNSPEC] = {.type = NLA_U8},
        [NL80211_BSS_STATUS] = {.type = NLA_U32},
        [NL80211_BSS_SEEN_MS_AGO] = {.type = NLA_U32},
        [NL80211_BSS_BEACON_IES] = {},
    };

    /* Parse and error check. */
    nla_parse(tb, NL80211_ATTR_MAX, genlmsg_attrdata(gnlh, 0), genlmsg_attrlen(gnlh, 0), NULL);
    if (!tb[NL80211_ATTR_BSS])
    {
        printf("bss info missing!\n");
        return NL_SKIP;
    }
    if (nla_parse_nested(bss, NL80211_BSS_MAX, tb[NL80211_ATTR_BSS], bss_policy))
    {
        printf("failed to parse nested attributes!\n");
        return NL_SKIP;
    }
    if (!bss[NL80211_BSS_BSSID])
        return NL_SKIP;
    if (!bss[NL80211_BSS_INFORMATION_ELEMENTS])
        return NL_SKIP;

    /* Start hadling received info.*/

    print_bssid(nla_data(bss[NL80211_BSS_BSSID]));

    print_ssid(nla_data(bss[NL80211_BSS_INFORMATION_ELEMENTS]), nla_len(bss[NL80211_BSS_INFORMATION_ELEMENTS]));

    print_freq(nla_get_u32(bss[NL80211_BSS_FREQUENCY]));

    print_channel(nla_data(bss[NL80211_BSS_INFORMATION_ELEMENTS]), nla_len(bss[NL80211_BSS_INFORMATION_ELEMENTS]));

    print_signal_db(nla_get_u32(bss[NL80211_BSS_SIGNAL_MBM]));

    print_bitrates(nla_data(bss[NL80211_BSS_INFORMATION_ELEMENTS]), nla_len(bss[NL80211_BSS_INFORMATION_ELEMENTS]));

    print_rsn_info(nla_data(bss[NL80211_BSS_INFORMATION_ELEMENTS]), nla_len(bss[NL80211_BSS_INFORMATION_ELEMENTS]));

    return NL_SKIP;
}

/*
 * Scan trigger.
 */
int wscanner_trigger(struct nl_sock *socket, int if_index, int driver_id)
{
    /* Starts the scan and waits for it to finish.
     * Does not return until the scan is done or has been aborted.
     */
    struct trigger_results results = {.done = 0, .aborted = 0};
    struct nl_cb *cb;
    struct nl_msg *msg, *ssids_to_scan = NULL;
    int err, res;
    int mcid = genl_ctrl_resolve_grp(socket, NL80211, SCAN);

    /* Without this, callback_trigger() won't be called. */
    nl_socket_add_membership(socket, mcid);

    /* Allocate the messages and callback handler. */
    msg = nlmsg_alloc();
    if (!msg)
    {
        printf("ERROR: Failed to allocate netlink message for msg.\n");
        return -1;
    }
    ssids_to_scan = nlmsg_alloc();
    if (!ssids_to_scan)
    {
        printf("ERROR: Failed to allocate netlink message for ssids_to_scan.\n");
        nlmsg_free(msg);
        return -1;
    }
    cb = nl_cb_alloc(NL_CB_DEFAULT);
    if (!cb)
    {
        printf("ERROR: Failed to allocate netlink callbacks.\n");
        nlmsg_free(msg);
        nlmsg_free(ssids_to_scan);
        return -1;
    }

    /* Setup the messages and callback handler. */
    /* Setup which command to run. */
    genlmsg_put(msg, 0, 0, driver_id, 0, 0, NL80211_CMD_TRIGGER_SCAN, 0);
    /* Add message attribute, which interface to use. */
    nla_put_u32(msg, NL80211_ATTR_IFINDEX, if_index);
    /* Scan all SSIDs. */
    nla_put(ssids_to_scan, 1, 0, "");
    /* Add message attribute, which SSIDs to scan for. */
    nla_put_nested(msg, NL80211_ATTR_SCAN_SSIDS, ssids_to_scan);
    /* Copied to `msg` above, no longer need this. */
    nlmsg_free(ssids_to_scan);
    /* Add the callbacks. */
    nl_cb_set(cb, NL_CB_VALID, NL_CB_CUSTOM, callback_trigger, &results);
    nl_cb_err(cb, NL_CB_CUSTOM, error_handler, &err);
    nl_cb_set(cb, NL_CB_FINISH, NL_CB_CUSTOM, finish_handler, &err);
    nl_cb_set(cb, NL_CB_ACK, NL_CB_CUSTOM, ack_handler, &err);
    /* No sequence checking for multicast messages. */
    nl_cb_set(cb, NL_CB_SEQ_CHECK, NL_CB_CUSTOM, no_seq_check, NULL);

    /* Send NL80211_CMD_TRIGGER_SCAN to start the scan.
    *  The kernel may reply with NL80211_CMD_NEW_SCAN_RESULTS on
    *  success or NL80211_CMD_SCAN_ABORTED if another scan was started by another process. */
    err = 1;
    /* Send the message. */
    res = nl_send_auto(socket, msg);

    /* First wait for ack_handler(). This helps with basic errors. */
    while (err > 0)
        res = nl_recvmsgs(socket, cb);
    if (err < 0)
        printf("WARNING: err has a value of %d.\n", err);

    if (res < 0)
    {
        printf("ERROR: nl_recvmsgs() returned %d (%s).\n", res, nl_geterror(-res));
        nlmsg_free(msg);
        return -1;;
    }

    /* Now wait until the scan is done or aborted. */
    while (!results.done)
        nl_recvmsgs(socket, cb);

    if (results.aborted)
    {
        printf("ERROR: Kernel aborted scan.\n");
        nlmsg_free(msg);
        return -1;
    }

    nlmsg_free(msg);
    nl_socket_drop_membership(socket, mcid);
    nl_cb_put(cb);
    return 0;
}

/*
 * Scanner init.
 */
int wscanner_init(char *device)
{
    printf("Device: %s\n\n", device);
    /* Use this wireless interface for scanning */
    socket_pars.if_index = if_nametoindex(device);
    /* Open socket to kernel */
    socket_pars.socket = nl_socket_alloc();
    /* Create file descriptor and bind socket */
    if (genl_connect(socket_pars.socket) < 0)
    {
        nl_socket_free(socket_pars.socket);
        return -1;
    }
    /* Find the nl80211 driver ID */
    socket_pars.driver_id = genl_ctrl_resolve(socket_pars.socket, "nl80211");

    return 0;
}

/*
 * Scan provider.
 */
int wscanner_scan_provide(char *device)
{
    struct nl_msg *msg;
    int res, rv = -1;

    printf("Device: %s scanning...\n\n", device);
    // Issue NL80211_CMD_TRIGGER_SCAN to the kernel and wait for it to finish.
    int err = wscanner_trigger(socket_pars.socket, socket_pars.if_index,
        socket_pars.driver_id);
    if (err != 0)
    {
        printf("wscanner_trigger() failed with %d.\n", err);
        goto Error;
    }

    /* Now get info for all SSIDs detected. */

    msg = nlmsg_alloc(); /* Allocate a message.*/
    /* Setup which command to run.*/
    genlmsg_put(msg, 0, 0, socket_pars.driver_id, 0, NLM_F_DUMP, NL80211_CMD_GET_SCAN, 0);
    /* Add message attribute, which interface to use. */
    nla_put_u32(msg, NL80211_ATTR_IFINDEX, socket_pars.if_index);
    /* Add the callback. */
    nl_socket_modify_cb(socket_pars.socket, NL_CB_VALID, NL_CB_CUSTOM, callback_data_handle, NULL);
    /* Send the message. */
    res = nl_send_auto(socket_pars.socket, msg);
    /* Retrieve the kernel's answer. callback_dump() prints SSIDs to stdout. */
    res = nl_recvmsgs_default(socket_pars.socket);
    nlmsg_free(msg);
    if (res < 0)
    {
        printf("ERROR: nl_recvmsgs_default() returned %d (%s).\n", res, nl_geterror(-res));
        goto Error;
    }
    rv = 0;
    return rv;

Error:
    nl_socket_free(socket_pars.socket);
    return rv;
}