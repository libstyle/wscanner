# wscanner
 - console application for scanning wireless networks using libnl
 - preconditions:
	- install:
	    - sudo apt-get install libnl-3-dev
	    - sudo apt-get install libnl-genl-3-dev
 - build: make
 - usage: sudo ./wscanner -d [WIFI_INTERFACE](e.g. sudo ./wscanner -d wlp2s0)