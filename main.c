#include "wscanner.h"

int main(int arg, char **argv)
{
    char device[_POSIX_ARG_MAX] = {0};
    int scanning = 1;

    if (wscanner_parse_args(arg, argv, device) < 0)
        return -1;

    if (wscanner_init(device) < 0)
        return -1;

    while (scanning)
    {
        if (wscanner_scan_provide(device) < 0)
            return -1;
    }

    return 0;
}
